package es.iescarrillo.android.ejemplobbdd.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import es.iescarrillo.android.ejemplobbdd.models.Book;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "BOOKAPP";
    private static final int DATABASE_VERSION = 2;

    //Sentencia SQL para crear la tabla de libros
    private static final String CREATE_BOOK_TABLE = "CREATE TABLE Book (_id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, author TEXT)";
    private static final String DROP_BOOK_TABLE = "DROP TABLE IF EXISTS Book";

    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Se ejecuta la sentencia SQL de creación de la tabla
        db.execSQL(CREATE_BOOK_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Ejecutar una consulta para obtener los datos de la tabla BOOK
        String[] arguments = new String[]{"El Quijote", "Francisco"};
        Cursor cursor = db.rawQuery("SELECT * FROM BOOK where title = 'El quijote'", arguments);

        List<Book> books = new ArrayList<>();
        if(cursor != null && cursor.moveToFirst()){
            do{
                Book book = new Book();
                book.setId(cursor.getInt(0));
                book.setTitle(cursor.getString(1));
                // etc.
                books.add(book);

            }while (cursor.moveToNext());
        }

        //Se ejecuta la sentencia SQL de eliminación de la tabla
        db.execSQL(DROP_BOOK_TABLE);
        //Llama l método anterior
        onCreate(db);
    }
}
