package es.iescarrillo.android.ejemplobbdd;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import es.iescarrillo.android.ejemplobbdd.datasources.BookDataSource;

public class DetailBookActivity extends AppCompatActivity {

    private Button btnUpdate,btnDelete, btnBack;

    private EditText etTitle, etAuthor;

    private BookDataSource bookDataSource;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_book);

        // Inicializamos datasource
        bookDataSource = new BookDataSource(this);

        btnUpdate = findViewById(R.id.btnUpdate);
        btnDelete = findViewById(R.id.btnDelete);
        btnBack = findViewById(R.id.btnBack);
        btnUpdate.setOnClickListener(v -> updateBook());
        btnDelete.setOnClickListener(v -> deleteBook());
        btnBack.setOnClickListener(v -> goBack());
        etTitle = findViewById(R.id.etTitle);
        etAuthor = findViewById(R.id.etAuthor);

        intent = getIntent();
        etTitle.setText(intent.getStringExtra("title"));
        etAuthor.setText(intent.getStringExtra("author"));
    }

    public void updateBook(){
        bookDataSource.updateBook(intent.getIntExtra("id", 0), etTitle.getText().toString(), etAuthor.getText().toString());
        goBack();
    }

    public void deleteBook(){
        bookDataSource.deleteBook(intent.getIntExtra("id", 0));
        goBack();
    }

    public void goBack(){
        Intent mainIntent = new Intent(this, MainActivity.class);
        startActivity(mainIntent);
    }
}