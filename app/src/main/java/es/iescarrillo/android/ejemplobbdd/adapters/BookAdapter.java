package es.iescarrillo.android.ejemplobbdd.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.SortedSet;
import java.util.stream.Collectors;

import es.iescarrillo.android.ejemplobbdd.R;
import es.iescarrillo.android.ejemplobbdd.models.Book;

public class BookAdapter extends ArrayAdapter<Book> {

    public BookAdapter(Context context, List<Book> books) {
        super(context, 0, books);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Obtén el objeto Contact en la posición actual
        Book book = getItem(position);

        // Reutiliza una vista existente o crea una nueva si es necesario
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_book, parent, false);
        }

        // Accede al TextView en el diseño de cada elemento del ListView
        TextView tvItem = convertView.findViewById(R.id.tvItem);
        tvItem.setText(book.getTitle() + " (" + book.getAuthor() + ")");

        return convertView;
    }
}
