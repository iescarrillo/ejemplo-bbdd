package es.iescarrillo.android.ejemplobbdd.datasources;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import es.iescarrillo.android.ejemplobbdd.database.DatabaseHelper;
import es.iescarrillo.android.ejemplobbdd.models.Book;

public class BookDataSource {

    // La base de base de datos
    private SQLiteDatabase database;
    // Lo usaremos para abrir y cerrar la conexión con la base de datos
    private DatabaseHelper dbHelper;

    //Constructor
    public BookDataSource(Context context){

        dbHelper = new DatabaseHelper(context);
    }

    //Abrir conexión
    public void openWritableDatabase() throws SQLException{
        database = dbHelper.getWritableDatabase();
    }

    public void openReadableDatabase() throws SQLException{
        database = dbHelper.getReadableDatabase();
    }

    //Cerrar conexión
    public void close(){
        dbHelper.close();
    }

    public void insertBook(String title, String author){
        openWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("title", title);

        if (author == null)
            values.put("author", "Anónimo");
        else
            values.put("author", author);

        database.insert("Book", null, values);

        close();
    }

    public void updateBook(int id, String title, String author){
        openWritableDatabase();

        // Solo añadimos los pares clave-valor de las columnas/atributos que queremos actualizar
        ContentValues values = new ContentValues();
        values.put("title", title);
        values.put("author", author);

        // Argumentos que serán en la clausula del where
        // String.valueOf(id) => casting de int a String
        String [] arguments= new String[]{String.valueOf(id)};
        database.update("Book", values, "_id=?", arguments);

        close();
    }

    public void deleteBook(int id){
        openWritableDatabase();

        String [] arguments= new String[]{String.valueOf(id)};
        database.delete("Book", "_id=?", arguments );

        close();
    }

    // Elimina todos los libros con menos de x páginas
    public void deleteBookLessPages(int rangeIni, int rangeEnd, String author){
        openWritableDatabase();

        String[] arguments = new String[]{String.valueOf(rangeIni), String.valueOf(rangeEnd), author};
        database.delete("Book", "numPage BETWEEEN ? and ? and author = ?", arguments);

        close();
    }

    public List<Book> getAllBooks(){
        List<Book> result = new ArrayList<>();

        // Abrir conexión
        openReadableDatabase();

        // Método database.query => hace la consulta parametrizable
        String[] columns = {"_id", "title", "author"};
        Cursor booksCursor = database.query("Book", columns, null, null, null, null, null);

        if(booksCursor != null && booksCursor.moveToFirst()){
            do{
                // Recuperar el índice de cada columna en BBDD
                int idIndex = booksCursor.getColumnIndex("_id");
                int titleIndex = booksCursor.getColumnIndex("title");
                int autorIndex = booksCursor.getColumnIndex("author");

                // Obtener los valores de las columnas
                int id = booksCursor.getInt(idIndex);
                String title = booksCursor.getString(titleIndex);
                String author = booksCursor.getString(autorIndex);

                Book book = new Book();
                book.setId(id);
                book.setAuthor(author);
                book.setTitle(title);
                result.add(book);

            }while (booksCursor.moveToNext());
        }

        // Cerrar conexión
        close();

        return result;
    }


}
