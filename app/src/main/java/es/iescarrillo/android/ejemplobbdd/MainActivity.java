package es.iescarrillo.android.ejemplobbdd;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.List;

import es.iescarrillo.android.ejemplobbdd.adapters.BookAdapter;
import es.iescarrillo.android.ejemplobbdd.database.DatabaseHelper;
import es.iescarrillo.android.ejemplobbdd.datasources.BookDataSource;
import es.iescarrillo.android.ejemplobbdd.models.Book;

public class MainActivity extends AppCompatActivity {

    Button btnInsert;
    EditText etTitle;
    EditText etAuthor;

    private BookDataSource bookDataSource;
    BookAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Inicializamos datasource
        bookDataSource = new BookDataSource(this);
        bookDataSource.getAllBooks();

        // Abrimos y cerramos la conexión para que compruebe si necesita ejecutar el
        // onCreate o onUpgrade
        bookDataSource.openWritableDatabase();
        bookDataSource.close();

        List<Book> books = bookDataSource.getAllBooks();
        ListView lvBooks = findViewById(R.id.lvBooks);
        adapter = new BookAdapter(this, books);
        lvBooks.setAdapter(adapter);

        etTitle = findViewById(R.id.etTitle);
        etAuthor = findViewById(R.id.etAuthor);

        btnInsert = findViewById(R.id.btnInsert);

        btnInsert.setOnClickListener(v -> insertBook());

        lvBooks.setOnItemClickListener((parent, view, position, id) -> {
            Book book = (Book) parent.getItemAtPosition(position);
            Intent intent = new Intent(this, DetailBookActivity.class);
            intent.putExtra("id", book.getId());
            intent.putExtra("title", book.getTitle());
            intent.putExtra("author", book.getAuthor());
            startActivity(intent);
        });
    }

    public void insertBook(){
        bookDataSource.insertBook(etTitle.getText().toString(), etAuthor.getText().toString());
        adapter.clear();
        adapter.addAll(bookDataSource.getAllBooks());
        adapter.notifyDataSetChanged();

        etAuthor.getText().clear();
        etTitle.getText().clear();
    }


}